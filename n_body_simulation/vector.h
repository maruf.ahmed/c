#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

typedef struct
{
    double x, y, z;
} vector;

vector addVectors (vector a, vector b);
vector substractVectors (vector a, vector b);
vector scalarMultiplyVector(vector a, double b);
int    compare (vector a, vector b);
double mod (vector a);

#endif // VECTOR_H_INCLUDED

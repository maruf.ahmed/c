# N body simulation - C implementation 

## References for further reading 

Parallel OpenMP and CUDA Implementations of the N -Body Problem    
https://tushaargvs.github.io/assets/publications/iccsa-2019-draft.pdf

The XStar N-body Solver Theory of Operation  
http://www.schlitt.net/xstar/n-body.pdf

N-Body Simulation   
https://www.cs.princeton.edu/courses/archive/fall12/cos126/checklist/nbody.html  

N-Body Simulation   
https://www.cs.princeton.edu/courses/archive/spr17/cos126/assignments/nbody.html  


#ifndef N_BODY_H_INCLUDED
#define N_BODY_H_INCLUDED


void initSystem (char *file, int *N, int *steps, double  *GravConst,
                 double **masses, vector **pos, vector **vel, vector **acc);

void simulation (int N, double  GravConst,
                 double **masses, vector **pos, vector **vel, vector **acc);


int printValues (int N, vector *pos, vector *vel, vector *acc);

#endif // N-BODY_H_INCLUDED

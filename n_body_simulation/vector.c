#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "vector.h"

vector addVectors (vector a, vector b)
{
    vector c = {a.x+b.x, a.y+b.y, a.z+b.z};
    return c;
}

vector substractVectors (vector a, vector b)
{
    vector c = { a.x-b.x, a.y-b.y, a.z-b.z};
    return c;
}

vector scalarMultiplyVector(vector a, double b)
{
    vector c = { a.x * b , a.y * b, a.z * b};
    return c;
}

double mod (vector a)
{
    return sqrt( a.x*a.x + a.y*a.y + a.z*a.z);
}


int compare (vector a, vector b)
{
    return ( a.x == b.x && a.y == b.y && a.z == b.z);
}

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "vector.h"
#include "n_body.h"

void initSystem (char *file, int *N, int *steps, double  *GravConst,
                 double **masses, vector **pos, vector **vel, vector **acc)

{

    FILE * fp = fopen (file, "r");
    if(!fp)
    {
        printf("initSystem: file open error\n");
        fclose(fp) ;
        exit(-1);
    }

    /* <Gravitational Constant> <Number of bodies(N)> <Time step> */
    fscanf(fp, "%lf%d%d", GravConst, N, steps);

    /* Allocate memory for array of vectors inside a function
     * REF:
     * realloc struct of array inside function
     * https://stackoverflow.com/questions/19457074/realloc-struct-of-array-inside-function
     * Is it possible to allocate array inside function and return it using reference?
     * https://stackoverflow.com/questions/13208673/is-it-possible-to-allocate-array-inside-function-and-return-it-using-reference
    */
    *masses = (double *) malloc (*N * sizeof (double));
    *pos    = (vector *) malloc (*N * sizeof (vector));
    *vel    = (vector *) malloc (*N * sizeof (vector));
    *acc    = (vector *) malloc (*N * sizeof (vector));

    if(!masses || !pos || !vel || !acc)
    {
        printf("initSystem: memory allocation error\n");
        fclose(fp) ;
        exit(-1);
    }

    for (int i=0; i < *N; i ++)
    {
        /* <Mass of M1> */
        fscanf(fp, "%lf", &(*masses)[i]);

        /* <Position of M1 in x,y,z co-ordinates> */
        fscanf(fp, "%lf%lf%lf", &(*pos)[i].x,  &(*pos)[i].y,  &(*pos)[i].z );

        /* <Initial velocity of M1 in x,,y,z components> */
        fscanf(fp, "%lf%lf%lf", &(*vel)[i].x, &(*vel)[i].y, &(*vel)[i].z );
    }

    fclose(fp) ;

    for (int i=0; i < *N; i ++)
    {
        (*acc)[i].x = (*acc)[i].y  = (*acc)[i].z = 0.0;
    }

    /* Print dynamically allocated vectors inside a function
     * Using pointer of pointers
    for(int i=0; i<*N; i++)
     {
         printf("Body %d: ", i);
         printf("%lf\t%lf\t%lf\t", (*pos)[i].x, (*pos)[i].y, (*pos)[i].z );
         printf("%lf\t%lf\t%lf\t\n", (*vel)[i].x, (*vel)[i].y, (*vel)[i].z );
     }
     */
}

void simulation (int N, double  GravConst,
                 double **masses, vector **pos, vector **vel, vector **acc)
{
     double temp;
     vector temp_vec;

     //printf("\n");
     //printValues (N, *pos, *vel, *acc);

     /* computeAccelerations */
     for (int i=0; i < N; i ++)
     {
         (*acc)[i].x = (*acc)[i].y = (*acc)[i].z = 0.0;
         for(int j = 0; j< N; j++)
         {
             if(i != j)
             {
                   temp_vec = substractVectors((*pos)[i], (*pos)[j]);
                   temp = GravConst * (*masses)[j] / pow ( mod ( temp_vec ) , 3 );
                   //printf (" %lf ", temp);
                   //vector t2 = scalarMultiplyVector(substractVectors ((*pos)[j], (*pos)[i]), temp);
                   //printf (" %lf %lf %lf \t", t2.x, t2.y, t2.z);
                   (*acc)[i] = addVectors ( (*acc)[i], scalarMultiplyVector(
                                                            substractVectors ((*pos)[j], (*pos)[i]), temp)  );
             }
         }
      }

     //printf("\n");
     //printValues (N, *pos, *vel, *acc);

	 /* computePositions     */
     for (int i=0; i < N; i ++)
     {
          (*pos)[i] = addVectors( (*pos)[i],
                                     addVectors( (*vel)[i], scalarMultiplyVector((*acc)[i], 0.5) ) );
     }

	 /* computeVelocities    */
     for (int i=0; i < N; i ++)
     {
          (*vel)[i] = addVectors( (*vel)[i],(*acc)[i]);
     }

	 /* resolveCollisions    */
     for (int i=0; i < N-1; i ++)
     {
         for(int j = i + 1; j< N; j++)
         {
             if( compare( (*pos)[i], (*pos)[j]) )
             {
                   vector temp_vec = (*vel)[i];
                   (*vel)[i] = (*vel)[j];
                   (*vel)[j] = temp_vec;
             }
         }
      }

}


int printValues (int N, vector *pos, vector *vel, vector *acc)
{
     if (!pos || !vel || !acc)
     {
         printf("printValues: One of the pointers is not valid\n");
         return -1;
     }

     for(int i=0; i<N; i++)
     {
         printf("%5d: ", i);
         printf("%12lf%12lf%12lf\t", pos[i].x, pos[i].y, pos[i].z );
         printf("%12lf%12lf%12lf\t", vel[i].x, vel[i].y, vel[i].z );

         printf("%12lf%12lf%12lf\n", acc[i].x, acc[i].y, acc[i].z );
     }
     return 0;
}

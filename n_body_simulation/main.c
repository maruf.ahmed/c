/************************
* N body simulation
*
* Author: Maruf
**************************/
/************************
* REF
* Parallel OpenMP and CUDA Implementations of the N -Body Problem
* https://tushaargvs.github.io/assets/publications/iccsa-2019-draft.pdf
* The XStar N-body Solver Theory of Operation
* http://www.schlitt.net/xstar/n-body.pdf
* N-Body Simulation
* https://www.cs.princeton.edu/courses/archive/fall12/cos126/checklist/nbody.html
* N-Body Simulation
* https://www.cs.princeton.edu/courses/archive/spr17/cos126/assignments/nbody.html
* Rosettacode N-body problem
* https://rosettacode.org/wiki/N-body_problem
**************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "vector.h"
#include "n_body.h"

int main (int argc, char *argv[])
{
    char    *file;
    int     N;
    int     steps;
    double  GravConst;
    double  *masses;
    vector  *pos, *vel, *acc;

    if(argc != 2)
    {
         printf("Usage : %s <data file>",argv[0]);
        exit(-1);
    }
    file = argv[1];

    /* Need to pass pointer to pointer for vector memory allocation inside the function */
    initSystem (file, &N, &steps, &GravConst,
                         &masses, &pos, &vel, &acc);

    //printf("%5s: " "%12s%12s%12s\t" "%12s%12s%12s\n", "Body","x","y", "z", "vx", "vy", "vz" );
    printf("%5s: " "%12s%12s%12s\t" "%12s%12s%12s\t" "%12s%12s%12s\n", "Body","x","y", "z", "vx", "vy", "vz", "ax", "ay", "az" );
    printValues (N, pos, vel, acc);

    for (int i=0; i < steps; i ++)
    {
        printf("Cycle %d ", i);
        printf("%.*s", 115, "==============================================================================="
        "===================================================================================================");
        printf("\n");
        simulation(N, GravConst,
                    &masses, &pos, &vel, &acc);

        //printf("%5s: " "%12s%12s%12s\t" "%12s%12s%12s\n", "Body","x","y", "z", "vx", "vy", "vz" );
        printf("%5s: " "%12s%12s%12s\t" "%12s%12s%12s\t" "%12s%12s%12s\n", "Body","x","y", "z", "vx", "vy", "vz", "ax", "ay", "az" );
        printValues (N, pos, vel, acc);
    }

    free(masses);
    free(pos);
    free(vel);
    free(acc);
    return 0;
}

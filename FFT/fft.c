
/**********************************
* REF:
* https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm
***********************************/


#include <stdio.h>
#include <math.h>
#include <complex.h>

double Pi;

void recursive_fft(double complex buffer[], double complex out[], int n, int steps)
{
     double complex temp;
     if (steps < n)
     {
         recursive_fft (out, buffer, n, steps*2 );
         recursive_fft (out+steps, buffer + steps, n, steps * 2);

         for (int i = 0; i < n; i += 2* steps )
         {
              temp = cexp ( -I * Pi * i/n ) * out [i + steps];
              buffer[i/2] = out [i] + temp;
              buffer[(i + n)/2] = out [i] - temp;

         }
     }
}

void handle_fft (double complex buffer [], int n)
{
    double complex out[n];
    for (int i =0; i < n; i ++)
        out [i] = buffer[i];

    recursive_fft(buffer, out, n, 1);
}

void show (const char *s, double complex buffer [], int n)
{
     printf("%s", s);
     for (int i=0; i < n; i ++)
     {
         if ( !cimag (buffer[i]) )
              printf ("%g ", creal (buffer[i]) );
         else
              printf ("(%g, %g) ", creal (buffer[i]), cimag(buffer[i]) );
     }
}

int main (int argc, char **argv)
{
    Pi = atan2 (1,1)*4;
    double complex buffer [] = {1,1,1,1,0,0,0,0};

    show ("Data: ", buffer, 8);
    handle_fft (buffer, 8);
    show ("\nFFT: ", buffer, 8);

    return 0;
}
